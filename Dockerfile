FROM node:10.3.0
EXPOSE 3000
WORKDIR /build
COPY src /build
COPY app_start.sh /build/app_start.sh
RUN npm install

FROM node:10.3.0
COPY --from=0 /build /app
CMD ["bash", "/app/app_start.sh"]
